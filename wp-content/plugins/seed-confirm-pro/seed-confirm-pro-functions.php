<?php
/**
 * Check woo-commerce plugin is installed and activated or not.
 * @return bool
 */
if (! function_exists('is_woo_activated')) {
    function is_woo_activated()
    {
        if (class_exists('woocommerce')) {
            return true;
        } else {
            return false;
        }
    }
}

/**
 * Get array of banks.
 * @param array $accounts
 * @return array
 */
if (!function_exists('seed_confirm_get_banks')) {
    function seed_confirm_get_banks($accounts)
    {
        $accounts_with_logos = array();

        if (!empty($accounts) && is_array($accounts) && count($accounts) > 0) {
            foreach ($accounts as $_account) {
                $logo = '';
                $bank_name = trim($_account['bank_name']);

                if ((false !==  mb_strpos($bank_name, 'กสิกร'))
                    || false !== mb_strpos($bank_name, '开泰银行')
                    || false !== stripos($bank_name, 'kbank')
                    || false !== stripos($bank_name, 'kasikorn')) {
                    $logo = 'kbank';
                } elseif ((false !== mb_strpos($bank_name, 'กรุงเทพ'))
                    || false !== mb_strpos($bank_name, '盘古银行')
                    || false !== stripos($bank_name, 'bbl')
                    || false !== stripos($bank_name, 'bangkok')
                    || false !== stripos($bank_name, 'bualuang')) {
                    $logo = 'bbl';
                } elseif ((false !== mb_strpos($bank_name, 'กรุงไทย'))
                    || false !== mb_strpos($bank_name, '泰京银行')
                    || false !== stripos($bank_name, 'ktb')
                    || false !== stripos($bank_name, 'krungthai')) {
                    $logo = 'ktb';
                } elseif ((false !== mb_strpos($bank_name, 'ทหารไทย'))
                    || false !== mb_strpos($bank_name, '泰国军人银行')
                    || false !== stripos($bank_name, 'tmb')
                    || false !== stripos($bank_name, 'thai military')) {
                    $logo = 'tmb';
                } elseif ((false !== mb_strpos($bank_name, 'ทีเอ็มบีธนชาต'))
                    || false !== mb_strpos($bank_name, 'ทีทีบี')
                    || false !== stripos($bank_name, 'ttb')
                    || false !== stripos($bank_name, 'thai military')) {
                    $logo = 'ttb';
                } elseif ((false !== mb_strpos($bank_name, 'ไทยพาณิชย์'))
                    || false !== mb_strpos($bank_name, '汇商银行')
                    || false !== stripos($bank_name, 'scb')
                    || false !== stripos($bank_name, 'siam commercial')) {
                    $logo = 'scb';
                } elseif ((false !== mb_strpos($bank_name, 'กรุงศรี'))
                    || false !== mb_strpos($bank_name, '大城银行')
                    || false !== stripos($bank_name, 'bay')
                    || false !== stripos($bank_name, 'krungsri')) {
                    $logo = 'krungsri';
                } elseif ((false !== mb_strpos($bank_name, 'ซิดี้'))
                    || false !== stripos($bank_name, 'citi')) {
                    $logo = 'citi';
                } elseif ((false !== mb_strpos($bank_name, 'ออมสิน'))
                    || false !== mb_strpos($bank_name, '政府储蓄银行')
                    || false !== stripos($bank_name, 'gsb')) {
                    $logo = 'gsb';
                } elseif ((false !== mb_strpos($bank_name, 'ธนชาต'))
                    || false !== mb_strpos($bank_name, '泰纳昌银行')
                    || false !== stripos($bank_name, 'tbank')) {
                    $logo = 'tbank';
                } elseif ((false !== mb_strpos($bank_name, 'ยูโอบี'))
                || false !== mb_strpos($bank_name, '大华银行')
                    || false !== stripos($bank_name, 'uob')) { 
                    $logo = 'uob';
                } elseif ((false !== mb_strpos($bank_name, 'อิสลาม'))
                    || false !== stripos($bank_name, 'islamic')
                    || false !== stripos($bank_name, 'ibank')) {
                    $logo = 'ibank';
                } elseif ((false !== mb_strpos($bank_name, 'อาคารสงเคราะห์'))
                    || false !== mb_strpos($bank_name, '住宅银行')
                    || false !== mb_strpos($bank_name, 'ธอส')
                    || false !== stripos($bank_name, 'ghb')) {
                    $logo = 'ghb';
                } elseif ((false !== mb_strpos($bank_name, 'เพื่อการเกษตร'))
                    || false !== mb_strpos($bank_name, '农业合作银行')
                    || false !== mb_strpos($bank_name, 'ธ.ก.ส.')
                    || false !== mb_strpos($bank_name, 'ธกส')
                    || false !== stripos($bank_name, 'baac')) {
                    $logo = 'baac';
                } elseif ((false !== mb_strpos($bank_name, 'ทิสโก้'))
                    || false !== mb_strpos($bank_name, '铁士古银行')
                    || false !== stripos($bank_name, 'tisco')) {
                    $logo = 'tisco';
                }elseif ((false !== mb_strpos($bank_name, 'เกียรตินาคิน'))
                    || false !== mb_strpos($bank_name, '甲那金银行')
                    || false !== stripos($bank_name, 'kiatnakin')
                    || false !== stripos($bank_name, 'kkp')) {
                    $logo = 'kkp';
                } elseif ((false !== mb_strpos($bank_name, 'ไอซีบีซี'))
                    || false !== mb_strpos($bank_name, '工商银行')
                    || false !== stripos($bank_name, 'icbc')) {
                    $logo = 'icbc';
                } elseif ((false !== mb_strpos($bank_name, 'แห่งประเทศจีน'))
                    || false !== mb_strpos($bank_name, '中国银行')
                    || false !== stripos($bank_name, 'bank of china')
                    || false !== stripos($bank_name, 'boc')) {
                    $logo = 'boc';
                } elseif ((false !== mb_strpos($bank_name, 'พร้อมเพย์'))
                    || false !== stripos($bank_name, 'promptpay')) {
                    $logo = 'promptpay';
                } elseif ((false !== mb_strpos($bank_name, 'ทรู'))
                    || false !== stripos($bank_name, 'true')) {
                    $logo = 'truemoney';
                } elseif (false !== stripos($bank_name, 'bca')) { /* ------------- INDONESIA BANKS ------------- */
                    $logo = 'bca';
                } elseif (false !== stripos($bank_name, 'bni')) {
                    $logo = 'bni';
                } elseif (false !== stripos($bank_name, 'bri')) {
                    $logo = 'bri';
                } elseif (false !== stripos($bank_name, 'mandiri')) {
                    $logo = 'mandiri';
                } elseif (false !== stripos($bank_name, 'btpn')) {
                    $logo = 'btpn';
                } elseif ((false !== mb_strpos($bank_name, 'การค้าต่างประเทศลาว')) /* ------------- LOAS BANKS ------------- */
                    || false !== mb_strpos($bank_name, 'ການຄ້າຕ່າງປະເທດລາວ')
                    || false !== stripos($bank_name, 'bcel')) { 
                    $logo = 'bcel';
                } elseif (false !== stripos($bank_name, 'aba')) { /* ------------- CAMOBDIA BANKS ------------- */
                    $logo = 'aba';
                } elseif (false !== stripos($bank_name, 'wing')) { 
                    $logo = 'wing';
                }

                if ($logo !== '') {
                    $_account['logo'] = plugins_url('img/'.$logo.'.png', __FILE__);
                } else {
                    $_account['logo'] = plugins_url('img/none.png', __FILE__);
                }

                $accounts_with_logos[] = $_account;
            }
        }

        return $accounts_with_logos;
    }
}

/**
 * Use for generate unique file name.
 * Difficult to predict.
 * Only slip image that upload through seed-confirm.
 * @param $dir
 * @param $name
 * @param $ext
 * @return (string) uniq name
 */
if (!function_exists('seed_unique_filename')) {
    function seed_unique_filename($dir, $name, $ext)
    {
        return 'slip-'.md5($dir.$name.time()).$ext;
    }
}


/**
 * Find Order ID from Order Number
 * @param $order_number
 * @return $order_id or false
 */

if (!function_exists('seed_get_order_id')) {
    function seed_get_order_id($order_number)
    {   
        $order_id = $order_number;
        if (!class_exists('woocommerce')) {
            return $order_id;
        }
        // Check for https://woocommerce.com/products/sequential-order-numbers-pro/
        if ( function_exists( 'wc_seq_order_number_pro' ) ) {
            $order_id = wc_seq_order_number_pro()->find_order_by_order_number($order_number);
            return $order_id;
        } else {
            // Other Plugins including https://wordpress.org/plugins/custom-order-numbers-for-woocommerce/
            $orders = wc_get_orders(array( 
                'limit' => 800, 
                'orderby' => 'date', 
                'order' => 'DESC',
                'post_status' => array('wc-on-hold', 'wc-processing', 'wc-checking-payment')
            ));
            foreach ($orders as $order) {
                if ($order->get_order_number() == $order_number) {
                    $order_id = $order->get_id();
                    break;
                } 
            }
            if (is_int($order_id)) {
                return $order_id;
            } else {
                return false;
            }
        }
    }
}



/**
 * Send Line Notify to Admin
 * @param $sc_id
 * @param $order_id
 * @param $image_path
 * @return (string) uniq name
 */
if (!function_exists('seed_confirm_send_line_admin')) {
    function seed_confirm_send_line_admin($sc_id, $order_id, $file_path)
    {
        // ดึงค่า Token มา
        $token = get_option('seed_confirm_line_notify_admin_token');
        if (!$token) return;

        // รายการแจ้งขำระเงิน
        $massage    = __('Confirm Payment', 'seed-confirm');
        $log        = get_post($sc_id);
        $massage   .= "\n" . trim(strip_tags(str_replace("<br>", "\n", $log->post_content)));
        $order      = new WC_Order($order_id);
        if ($order->get_order_number()) {
            $massage   .= "\n" . __('Link', 'seed-confirm') . ': ' . get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
        } else {
            $massage   .= "\n\n" . __('No Order Found!', 'seed-confirm');
            $massage   .= "\n" . __('Please check', 'seed-confirm') . ' ' . get_admin_url() . 'post.php?post=' . $sc_id . '&action=edit';
        }
        // เช็คว่าไฟล์แนบเป็นรูปหรือเปล่า?
        if (strpos($file_path,'.png') || strpos($file_path,'.jpg') || strpos($file_path,'.jpeg')) {
            $image_path = $file_path;
        } else {
            $image_path = '';
        }
        seed_confirm_line_notify($massage, $image_path, $token);
    }
}

/**
 * Send Line Notify to Shipping
 * @param $order_id
 * @return (string) uniq name
 */
function seed_confirm_send_line_shipping($order_id)
{
    // ดึงค่า Token มา
    $token = get_option('seed_confirm_line_notify_shipping_token');
    if (!$token) return;

    // รายการออร์เดอร์
    $order = new WC_Order($order_id);
    if (!$order->get_order_number()) return;

    // ชื่อจังหวัด
    $province = array(
		'TH-81' => 'กระบี่',
		'TH-10' => 'กรุงเทพมหานคร',
		'TH-71' => 'กาญจนบุรี',
		'TH-46' => 'กาฬสินธุ์',
		'TH-62' => 'กำแพงเพชร',
		'TH-40' => 'ขอนแก่น',
		'TH-22' => 'จันทบุรี',
		'TH-24' => 'ฉะเชิงเทรา',
		'TH-20' => 'ชลบุรี',
		'TH-18' => 'ชัยนาท',
		'TH-36' => 'ชัยภูมิ',
		'TH-86' => 'ชุมพร',
		'TH-57' => 'เชียงราย',
		'TH-50' => 'เชียงใหม่',
		'TH-92' => 'ตรัง',
		'TH-23' => 'ตราด',
		'TH-63' => 'ตาก',
		'TH-26' => 'นครนายก',
		'TH-73' => 'นครปฐม',
		'TH-48' => 'นครพนม',
		'TH-30' => 'นครราชสีมา',
		'TH-80' => 'นครศรีธรรมราช',
		'TH-60' => 'นครสวรรค์',
		'TH-12' => 'นนทบุรี',
		'TH-96' => 'นราธิวาส',
		'TH-55' => 'น่าน',
		'TH-38' => 'บึงกาฬ',
		'TH-31' => 'บุรีรัมย์',
		'TH-13' => 'ปทุมธานี',
		'TH-77' => 'ประจวบคีรีขันธ์',
		'TH-25' => 'ปราจีนบุรี',
		'TH-94' => 'ปัตตานี',
		'TH-14' => 'พระนครศรีอยุธยา',
		'TH-56' => 'พะเยา',
		'TH-82' => 'พังงา',
		'TH-93' => 'พัทลุง',
		'TH-66' => 'พิจิตร',
		'TH-65' => 'พิษณุโลก',
		'TH-76' => 'เพชรบุรี',
		'TH-67' => 'เพชรบูรณ์',
		'TH-54' => 'แพร่',
		'TH-83' => 'ภูเก็ต',
		'TH-44' => 'มหาสารคาม',
		'TH-49' => 'มุกดาหาร',
		'TH-58' => 'แม่ฮ่องสอน',
		'TH-35' => 'ยโสธร',
		'TH-95' => 'ยะลา',
		'TH-45' => 'ร้อยเอ็ด',
		'TH-85' => 'ระนอง',
		'TH-21' => 'ระยอง',
		'TH-70' => 'ราชบุรี',
		'TH-16' => 'ลพบุรี',
		'TH-52' => 'ลำปาง',
		'TH-51' => 'ลำพูน',
		'TH-42' => 'เลย',
		'TH-33' => 'ศรีสะเกษ',
		'TH-47' => 'สกลนคร',
		'TH-90' => 'สงขลา',
		'TH-91' => 'สตูล',
		'TH-11' => 'สมุทรปราการ',
		'TH-75' => 'สมุทรสงคราม',
		'TH-74' => 'สมุทรสาคร',
		'TH-27' => 'สระแก้ว',
		'TH-19' => 'สระบุรี',
		'TH-17' => 'สิงห์บุรี',
		'TH-64' => 'สุโขทัย',
		'TH-72' => 'สุพรรณบุรี',
		'TH-84' => 'สุราษฎร์ธานี',
		'TH-32' => 'สุรินทร์',
		'TH-43' => 'หนองคาย',
		'TH-39' => 'หนองบัวลำภู',
		'TH-15' => 'อ่างทอง',
		'TH-37' => 'อำนาจเจริญ',
		'TH-41' => 'อุดรธานี',
		'TH-53' => 'อุตรดิตถ์',
		'TH-61' => 'อุทัยธานี',
		'TH-34' => 'อุบลราชธานี'
	);

    $dt = $order->get_data();

    $massage    = __("Order No.", "seed-confirm") . ": " . $order->get_order_number();
    $massage   .= "\n" . __("Date", "seed-confirm") . ": " . date_i18n(get_option('date_format') . ' • ' . get_option( 'time_format' ), strtotime(get_post($order->get_id())->post_date));
    $massage   .= "\n" . __("Status", "seed-confirm") . ": " . esc_html( wc_get_order_status_name( $order->get_status() ) );
    $massage   .= "\n" . __("Total", "seed-confirm") . ": " . $dt['total'];
    $massage   .= "\n" . __("Item(s)", "seed-confirm") . ": " . $order->get_item_count() . "\n";

    foreach ( $order->get_items() as $item_id => $item ) {
        $product    = $item->get_product();
        $sku        = $product->get_sku();
        $name       = $item->get_name();
        $quantity   = $item->get_quantity();
        $total      = $item->get_total();
        $massage .= "\n";
        if ($sku) $massage .= "[" . $sku . "] ";
        $massage .= $name . " × " . $quantity . " = " . $total;
    }

    if ($dt['shipping']['first_name']) {
        $shipping = 'shipping';
    } else {
        $shipping = 'billing';
    }

    $name   = $dt[$shipping]['first_name'] . ' ' . $dt[$shipping]['last_name'];
    $addr   = $dt[$shipping]['address_1'];
    if ($dt[$shipping]['address_2']) $addr .= "\n" . $dt[$shipping]['address_2'] . ' ';
    $addr  .= $dt[$shipping]['city'] . ' ';
    if ($dt[$shipping]['country'] == 'TH') {
        $addr  .= $province[$dt[$shipping]['state']];
    } else {
        $addr  .= $dt[$shipping]['state'];
    }
    $addr  .= "\n" . $dt[$shipping]['postcode'];
    if ($dt[$shipping]['country'] && $dt[$shipping]['country'] != 'TH') $addr .= "\n" . $dt[$shipping]['country'];
    
    $massage   .= "\n\n" . __("Shipping", "seed-confirm");
    $massage   .= "\n" . __("Name", "seed-confirm") . ": " . $name;
    $massage   .= "\n" . __("Phone", "seed-confirm") . ": " . $dt['billing']['phone'];
    
    if ( $dt[$shipping]['company']) {
        $massage  .= "\n" . __("Company", "seed-confirm") . ": " .  $dt[$shipping]['company'];    
    }

    $massage   .= "\n" . __("Address", "seed-confirm") . ": " . $addr;


    if ($order->get_customer_note()) {
        $massage   .= "\n\n" . __("Note", "seed-confirm");
        $massage   .= "\n" . $order->get_customer_note();
    }
    
    $massage   .= "\n" . __('Link', 'seed-confirm') . ': ' . get_admin_url() . 'post.php?post=' . $order_id . '&action=edit';
    
    $image_path = '';
    seed_confirm_line_notify($massage, $image_path, $token);
    
}


/**
 * Send Line Notify
 */
if (!function_exists('seed_confirm_line_notify')) {
    function seed_confirm_line_notify( $massage, $image_path, $token ){
        $api_url = 'https://notify-api.line.me/api/notify';
        if( $image_path != '' ) {
            $massage_push = array(
                'imageFile' => curl_file_create( $image_path ),
                'message' => $massage
            );
        } else {
            $massage_push = array(
                'message' => $massage
            );	
        }
        $curl = curl_init();
        curl_setopt( $curl, CURLOPT_URL, $api_url );
        curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, 'POST' );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt( $curl, CURLOPT_POSTFIELDS, $massage_push );
        curl_setopt( $curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: multipart/form-data',
            'Authorization: Bearer ' . $token
        ) );
        $result_callback = curl_exec( $curl );
        if( curl_errno( $curl ) ){
            wp_die( __( curl_error( $curl ) ), __( 'Error' ), array( 'response' => 403 ) );
        }
        curl_close( $curl );
    }
}



