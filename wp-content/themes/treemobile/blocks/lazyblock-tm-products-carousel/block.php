<?php
/**
 * Block: Products Carousel
 * 
 * บล็อกนี้เป็น custom block พิเศษ ที่ทำมาเพื่อใช้งานในหน้า frontpage
 * โดยให้คล้ายกับดีไซน์ที่ออกแบบมามากที่สุด ดังนั้นบล็อกนี้อาจจะไม่ได้ยืดหยุ่นมากนัก
 * 
 * @var $attributes
 * @var $block
 * @var $context
 */
$block_id = $attributes['blockId'];

$settings = wp_parse_args( $attributes, [
  'id'                    => '',
  'title'                 => '',
  'title_tag'             => 'h2',
  'title_new'             => '',
  'title_bestseller'      => '',
  'show_tab_new'          => '1',
  'show_tab_bestseller'   => '1',
  'product_cat'           => '',
  'product_tag'           => '',
  'numofposts'            => 10,
] );

$cid = $settings['id'] ?: "tm_products_carousel_{$block_id}";

$shortcode_tag = wp_sprintf( '[tm_products_carousel id="%1$s" title="%2$s" title_new="%3$s" title_bestseller="%4$s" title_tag="%5$s" show_tab_new="%6$s" show_tab_bestseller="%7$s" product_cat="%8$s" product_tag="%9$s" numofposts="%10$s"]',
  $cid,
  $settings['title'],
  $settings['title_new'],
  $settings['title_bestseller'],
  $settings['title_tag'],
  $settings['show_tab_new'],
  $settings['show_tab_bestseller'],
  $settings['product_cat'],
  $settings['product_tag'],
  $settings['numofposts'],
);

echo do_shortcode( $shortcode_tag );