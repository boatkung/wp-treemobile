<?php
/**
 * Block: Action Tabs
 * 
 * @var $attributes
 * @var $block
 * @var $context
 */
$list = $attributes['list'];
?>

<?php if ($list) : ?>
<div class="tm-block-action-tabs">
  <ul class="tm-block-action-tabs__list">
    <?php foreach( $list as $item ) : ?>
    <?php
    $label       = isset($item['label']) ? $item['label'] : '';
    $url         = isset($item['url']) ? $item['url'] : '';
    $target      = isset($item['new_tab']) ? '_blank' : '_self';
    $icon        = isset($item['icon']) ? $item['icon'] : '';
    $customClass = isset($item['custom_class']) ? $item['custom_class'] : '';
    $listClass   = 'tm-block-action-tabs__item';
    if ($customClass) {
      $listClass .= " tm-block-action-tabs__item--{$customClass}";
    }
    ?>
    <li class="<?= $listClass ?>">
      <a href="<?= $url ?>" target="<?= $target ?>">
        <?php if ($icon) : ?>
        <span class="icon"><?php echo do_shortcode( $icon ); ?></span>
        <?php endif; ?>

        <span class="text"><?= $label ?></span>

        <span class="icon-arrow"><i class="fal fa-angle-right"></i></span>
      </a>
    </li>
    <?php endforeach ?>
  </ul>
</div>
<?php endif ?>