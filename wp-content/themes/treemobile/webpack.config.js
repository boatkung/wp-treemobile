const path = require('path');

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /.js$/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env'],
        },
      },
    ],
  },
  output: {
    filename: 'treemobile.js',
  },
  externals: [
    {
      'jquery': 'jQuery',
      'swiper': 'Swiper',
      'lodash': 'lodash',
      '@wordpress/hooks': 'wp.hooks',
    }
  ]
}