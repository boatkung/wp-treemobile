<?php



add_shortcode( 'tm_wp_image', 'tm_wp_image_shortcode' );
add_shortcode( 'tm_recaptcha_notice', 'tm_recaptcha_notice_shortcode' );
add_shortcode( 'tm_benefit_bar', 'tm_benefit_bar_shortcode' );
add_shortcode( 'tm_benefit_bar_white', 'tm_benefit_bar_white_shortcode' );
add_shortcode( 'tm_products_carousel', 'tm_products_carousel_shortcode' );
add_shortcode( 'tm_alternative_shop', 'tm_alternative_shop_shortcode' );



function tm_wp_image_shortcode( $atts ) {
    $attr = shortcode_atts( [
        'id' => 0,
        'size' => 'thumbnail',
    ], $atts, 'tm_wp_image' );

    return wp_get_attachment_image( $attr['id'], $attr['size'] );
}



function tm_recaptcha_notice_shortcode() {
    ob_start();
    echo display_google_recaptcha_notice();
    return ob_get_clean();
}



function display_google_recaptcha_notice() {
    $privacy_url = 'https://policies.google.com/privacy';
    $terms_url   = 'https://policies.google.com/terms';
    /** Translators: %1$s: Recaptcha privacy url, %2$s: Recaptcha terms url */
    echo wp_sprintf( _x('This site is protected by reCAPTCHA and the Google <a href="%1$s">Privacy Policy</a> and <a href="%2$s">Terms of Service</a> apply.', 'google-recaptcha', 'treemobile'),
      $privacy_url,
      $terms_url );
  }



function tm_benefit_bar_shortcode() {
  ob_start();
?>
<div class="block-benefit-bar block-benefit-bar--green">
    <div class="item">
        <span class="symbol">
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop-white.png' ?>"
                srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop-white.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop-white@2x.png' ?> 2x"
                alt="Shopping"
                width="48"
                height="56">
        </span>
        <span class="text">
            <b>ส่งฟรี ส่งเร็ว</b><br/>
            ทั่วประเทศ
        </span>
    </div>

    <div class="item">
        <span class="symbol">
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free-white.png' ?>"
                srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free-white.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free-white@2x.png' ?> 2x"
                alt="Free Delivery"
                width="64"
                height="56">
        </span>
        <span class="text">
            <b>ประกันศูนย์</b><br/>
            ทั่วประเทศ
        </span>
    </div>

    <div class="item">
        <span class="symbol">
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee-white.png' ?>"
                srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee-white.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee-white@2x.png' ?> 2x"
                alt="Warantee"
                width="53"
                height="56">
        </span>
        <span class="text">
            <b>ง่าย สะดวก ปลอดภัย</b><br/>
            ตลอด 24 ช.ม.
        </span>
    </div>
</div>
<?php
  return ob_get_clean();
}



function tm_benefit_bar_white_shortcode() {
  ob_start();
?>
<div class="block-benefit-bar block-benefit-bar--white">
    <div class="item">
        <span class="symbol">
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop.png' ?>"
                srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop@2x.png' ?> 2x"
                alt="Shopping"
                width="48"
                height="56">
        </span>
        <span class="text">
            <b>ส่งฟรี ส่งเร็ว</b><br/>
            ทั่วประเทศ
        </span>
    </div>

    <div class="item">
        <span class="symbol">
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free.png' ?>"
                srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free@2x.png' ?> 2x"
                alt="Free Delivery"
                width="64"
                height="56">
        </span>
        <span class="text">
            <b>ประกันศูนย์</b><br/>
            ทั่วประเทศ
        </span>
    </div>

    <div class="item">
        <span class="symbol">
            <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee.png' ?>"
                srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee@2x.png' ?> 2x"
                alt="Warantee"
                width="53"
                height="56">
        </span>
        <span class="text">
            <b>ง่าย สะดวก ปลอดภัย</b><br/>
            ตลอด 24 ช.ม.
        </span>
    </div>
</div>
<?php
  return ob_get_clean();
}



function tm_products_carousel_shortcode( $atts = [] ) {
    $attr = shortcode_atts( [
        'id'                    => '',
        'title'                 => '',
        'title_tag'             => 'h2',
        'title_new'             => '',
        'title_bestseller'      => '',
        'show_tab_new'          => '1',
        'show_tab_bestseller'   => '1',
        'product_cat'           => '',
        'product_tag'           => '',
        'numofposts'            => 10,
    ], $atts, 'tm_products_carousel' );

    $id = (empty($attr['id'])) ? md5( http_build_query($attr) ) : $attr['id'];

    $new_query = [
        'tax_query' => [],
    ];

    $bestseller_query = [
        'tax_query' => [
            'relation' => 'AND',
            [
                'taxonomy' => 'product_tag',
                'field'    => 'slug',
                'terms'    => 'best-seller',
            ],
        ],
    ];

    if (!empty($cats = $attr['product_cat'])) {
        $catsArr = explode(',', $cats);

        $new_query['tax_query'][] = [
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => $catsArr,
        ];

        $bestseller_query['tax_query'][] = [
            [
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => $catsArr,
            ],
        ];
    }

    if (!empty($tags = $attr['product_tag'])) {
        $tagsArr = explode(',', $tags);

        $new_query['tax_query'][] = [
            'taxonomy' => 'product_tag',
            'field' => 'slug',
            'terms' => $tagsArr,
        ];

        $bestseller_query['tax_query'][] = [
            [
                'taxonomy' => 'product_tag',
                'field'    => 'slug',
                'terms'    => $tagsArr,
            ],
        ];
    }

    $new        = tm_get_products( $new_query );
    $bestseller = tm_get_products( $bestseller_query );
?>
<div class="products-carousel" id="products_carousel_<?= $id ?>">
    <?php echo wp_sprintf('<%2$s>%1$s</%2$s>', $attr['title'], $attr['title_tag']); ?>

    <div class="products-content-wrapper">
        <ul class="nav product-tabs" role="tablist">
            <?php if ($attr['show_tab_new'] === '1') : ?>
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="tab_new_<?= $id ?>" data-bs-toggle="tab" data-bs-target="#new_<?= $id ?>" type="button" role="tab" aria-controls="new_<?= $id ?>" aria-selected="true">
                    <?= $attr['title_new'] ?>
                </button>
            </li>
            <?php endif; ?>

            <?php if ($attr['show_tab_bestseller'] === '1') : ?>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="tab_pick_<?= $id ?>" data-bs-toggle="tab" data-bs-target="#pick_<?= $id ?>" type="button" role="tab" aria-controls="pick_<?= $id ?>" aria-selected="false">
                    <?= $attr['title_bestseller'] ?>
                </button>
            </li>
            <?php endif; ?>
        </ul>

        <div class="tab-content products-content">
            <?php if ($attr['show_tab_new'] === '1') : ?>
            <div class="tab-pane fade show active" id="new_<?= $id ?>" role="tabpanel" aria-labelledby="tab_new_<?= $id ?>">
                <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <?php get_template_part( 'template-parts/product', 'slide-item' ); ?>
                    </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(  ); ?>
                </div>
                </div>
            </div>
            <?php endif; ?>
        
            <?php if ($attr['show_tab_bestseller'] === '1') : ?>
            <div class="tab-pane fade" id="pick_<?= $id ?>" role="tabpanel" aria-labelledby="tab_pick_<?= $id ?>">
                <div class="swiper-container">
                <div class="swiper-wrapper">
                    <?php while ($bestseller->have_posts()) : $bestseller->the_post(); ?>
                    <div class="swiper-slide">
                        <?php get_template_part( 'template-parts/product', 'slide-item' ); ?>
                    </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(  ); ?>
                </div>
                </div>
            </div>
            <?php endif; ?>
        </div>

        <div class="products-navigation">
            <div class="products-navigation-button products-navigation-button--prev"><i class="icon fal fa-chevron-left"></i></div>
            <div class="products-navigation-button products-navigation-button--next"><i class="icon fal fa-chevron-right"></i></div>
        </div>
    </div>
</div>
<?php
}



/**
 * Shortcode: Alternative Stop
 * แสดงปุ่มลิงก์ไปยังช่องทางการสั่งซื้ออื่นนอกจากเว็บไซต์
 *
 * @return html
 */
function tm_alternative_shop_shortcode() {
    ?>
    <div class="tm-alternative-shop">
        <h3 class="tm-alternative-shop__title">ช่องทางการสั่งซื้ออื่น ๆ</h3>
        <div class="tm-alternative-shop__link">
            <a class="link link--facebook" href="#"><i class="fab fa-facebook"></i> สั่งซื้อผ่านทาง Facebook</a>
            <a class="link link--line" href="#"><i class="fab fa-line"></i> สั่งซื้อผ่านทาง Line</a>
        </div>
    </div>
    <?php
}