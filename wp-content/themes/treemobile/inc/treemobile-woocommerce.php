<?php



if (!function_exists('tm_get_product_price')) {
    function tm_get_product_price($product) {
        if ( is_array( $product ) ) {
            $product = $product;
        } else {
            $product = wc_get_product( $product );
        }

        if ( !$product ) {
            return;
        }

        $currency        = get_woocommerce_currency();
        $currency_symbol = get_woocommerce_currency_symbol($currency);
        $price_format    = get_woocommerce_price_format();
        $price           = wc_get_price_to_display($product);
        $dec             = wc_get_price_decimals();
        $sep             = wc_get_price_thousand_separator();
        $sep_text        = wc_get_price_decimal_separator();
        $output          = wp_sprintf($price_format, $currency_symbol, number_format($price, $dec, $sep_text, $sep));
        
        return $output;
    }
}



/**
 * Modify account menu items
 *
 * @param Array $menu_links
 * @return void
 */
function treemobile_modify_account_menu_items( $menu_links ) {
    // Remove donwload menu
    unset($menu_links['downloads']);

    $menu_ordered = [];

    $payment_confirmation_page_id       = apply_filters( 'payment_confirmation_page_id', 509 );
    $payment_confirmation_page          = get_post($payment_confirmation_page_id);
    $payment_confirmation_page_title    = $payment_confirmation_page->post_title;
    $payment_confirmation_page_name     = $payment_confirmation_page->post_name;

    foreach( $menu_links as $key => $value ) {
        $menu_ordered[$key] = $value;

        if ($key === 'orders') {
            $menu_ordered[$payment_confirmation_page_name] = $payment_confirmation_page_title;
        }
    }

    return $menu_ordered;
}

add_filter( 'woocommerce_account_menu_items', 'treemobile_modify_account_menu_items' );




function tm_get_products( $query = [] ) {
    $q = wp_parse_args( $query, [
        'post_type'     => 'product',
        'numofposts'    => 10,
        'order'         => 'DESC',
        'orderby'       => 'date',
        'post_status'   => 'publish',
    ] );
  
    return new WP_Query($q);
}
