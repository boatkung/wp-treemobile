<?php
// Add custom Theme Functions here

add_action( 'after_setup_theme', 'treemobile_setup' );
add_action( 'wp_enqueue_scripts', 'treemobile_scripts' );
add_action( 'admin_enqueue_scripts', 'treemobile_admin_scripts' );

add_filter( 'block_categories', 'treemobile_block_categories' );
add_filter( 'nav_menu_item_title', 'treemobile_menu_item_shortcode_support' );
add_filter( 'wpcf7_form_elements', 'add_shortcode_support_for_contact_form_7' );



function treemobile_setup() {
  add_theme_support( 'align-wide' );
}




function get_stylesheet_filemtime( $path ) {
  return filemtime( get_stylesheet_directory(  ) . $path );
}




function treemobile_scripts() {
  wp_enqueue_script( 'swiper', get_stylesheet_directory_uri(  ) . '/assets/lib/swiper/swiper-bundle.min.js', [], '6.7.0', true );
  wp_enqueue_style( 'swiper', get_stylesheet_directory_uri(  ) . '/assets/lib/swiper/swiper-bundle.min.css', [], '6.7.0' );
  wp_enqueue_script( 'sharer', get_stylesheet_directory_uri(  ) . '/assets/lib/sharer.js/sharer.min.js', [], '0.4.1', true );

  wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri(  ) . '/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js', [], '5.0.2', true );
  wp_enqueue_style( 'treemobile', get_stylesheet_directory_uri(  ) . '/assets/css/treemobile.css', [], get_stylesheet_filemtime('/assets/css/treemobile.css') );
  wp_enqueue_script( 'treemobile', get_stylesheet_directory_uri(  ) . '/assets/js/treemobile.js', ['jquery', 'lodash'], get_stylesheet_filemtime('/assets/js/treemobile.js'), true );

  wp_localize_script( 'treemobile', 'TREEMOBILE', [
    'wp' => [
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
      'resturl' => rest_url(),
    ],
  ] );
}



function treemobile_admin_scripts() {
  wp_enqueue_script( 'bootstrap', get_stylesheet_directory_uri(  ) . '/assets/lib/bootstrap/dist/js/bootstrap.bundle.min.js', ['jquery'], '5.0.2', true );
  wp_enqueue_script( 'swiper', get_stylesheet_directory_uri(  ) . '/assets/lib/swiper/swiper-bundle.min.js', [], '6.7.0', true );
  wp_enqueue_style( 'swiper', get_stylesheet_directory_uri(  ) . '/assets/lib/swiper/swiper-bundle.min.css', [], '6.7.0' );

  wp_enqueue_script( 'treemobile-admin', get_stylesheet_directory_uri(  ) . '/assets/js/treemobile.admin.js', ['wp-hooks', 'jquery', 'bootstrap', 'swiper'], get_stylesheet_filemtime('/assets/js/treemobile.admin.js'), true );
  wp_enqueue_style( 'treemobile-admin', get_stylesheet_directory_uri(  ) . '/assets/css/treemobile.admin.css', ['swiper'], get_stylesheet_filemtime('/assets/css/treemobile.admin.css') );
}



/**
 * ทำให้สามารถใส่ Shortcode ลงไปในตัวสร้างฟอร์มของ Contact Form 7 ได้โดยตรง
 * @see https://wordpress.stackexchange.com/questions/45266/how-to-use-other-shortcodes-inside-contact-form-7-forms
 */
function add_shortcode_support_for_contact_form_7( $form ) {
  $form = do_shortcode( $form );
  return $form;
}


/**
 * treemobile_menu_item_shortcode_support function
 *
 * @param String $title
 * @return void
 */
function treemobile_menu_item_shortcode_support( $title ) {
  return do_shortcode( $title );
}



add_action( 'pre_get_posts', function( $query ) {
  $sale_items = wc_get_product_ids_on_sale();
  
  if ( !is_admin() && $query->is_main_query() ) {
    $campaign = isset($_GET['campaign']) ? $_GET['campaign'] : NULL;

    // $query->set('post__in', $sale_items);
  }
} );



class TM_Widget_Filter_On_Sale extends WP_Widget {
  function __construct() {
    parent::__construct( 'tm_filter_on_sale', 'Treemobile Filter: On Sale' );

    add_action( 'widgets_init', function() {
      register_widget( 'TM_Widget_Filter_On_Sale' );
    } );
  }

  public $args = array(
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>'
  );

  public function widget( $args, $instance ) {
    echo $args['before_widget'];

    if ( ! empty( $instance['title'] ) ) {
        echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
    }

    echo '<div class="textwidget">';

    ?>
    <form>
      <label>
        <span class="field"><input type="checkbox" name="on_sale" value="1" /></span>
        <span class="text">กำลังลดราคา</span>
      </label>
    </form>
    <?php

    echo '</div>';

    echo $args['after_widget'];
  }

  public function form( $instance ) {
 
    $title = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( '', 'text_domain' );
    $text = ! empty( $instance['text'] ) ? $instance['text'] : esc_html__( '', 'text_domain' );
?>
    <p>
    <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php echo esc_html__( 'Title:', 'text_domain' ); ?></label>
        <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <p>
        <label for="<?php echo esc_attr( $this->get_field_id( 'Text' ) ); ?>"><?php echo esc_html__( 'Text:', 'text_domain' ); ?></label>
        <textarea class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" cols="30" rows="10"><?php echo esc_attr( $text ); ?></textarea>
    </p>
<?php

  }

  public function update( $new_instance, $old_instance ) {

    $instance = array();

    $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['text'] = ( !empty( $new_instance['text'] ) ) ? $new_instance['text'] : '';

    return $instance;
  }
}

new TM_Widget_Filter_On_Sale();



add_action( 'pre_get_posts', 'treemobile_modify_query' );

function treemobile_modify_query( $query ) {
  $on_sale = $_GET['tm_sale'] ?? 0;

  if ( $on_sale && $query->is_main_query() && is_post_type_archive( 'product' ) ) {
    $query->set('post__in', wc_get_product_ids_on_sale());
  }
}



require_once get_stylesheet_directory(  ) . '/inc/treemobile-blocks.php';
require_once get_stylesheet_directory(  ) . '/inc/treemobile-shortcodes.php';
require_once get_stylesheet_directory(  ) . '/inc/treemobile-woocommerce.php';
