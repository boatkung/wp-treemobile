<?php get_header(); ?>


<?php
$products = new WP_Query([
  'post_type'   => 'product',
  'numofposts'  => 10,
  'order'       => 'DESC',
  'orderby'     => 'date',
  'post_status' => 'publish',
]);
?>



<?php do_action( 'flatsome_before_page' ); ?>

<div id="content" role="main" class="content-area blocks-area">

		<div class="tm-block-hero-cover alignfull fluid">
      <?php masterslider("home-slider"); ?>
    </div>



    <div class="alignfull" id="benefits_bar">
      <div class="item">
        <span class="symbol">
          <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop.png' ?>"
            srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/shop@2x.png' ?> 2x"
            alt="Shopping" width="48" height="56">
        </span>
        <span class="text">
          <b>ส่งฟรี ส่งเร็ว</b><br />
          ทั่วประเทศ
        </span>
      </div>

      <div class="item">
        <span class="symbol">
          <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free.png' ?>"
            srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/free.png' ?> 2x"
            alt="Free Delivery" width="64" height="56">
        </span>
        <span class="text">
          <b>ประกันศูนย์</b><br />
          ทั่วประเทศ
        </span>
      </div>

      <div class="item">
        <span class="symbol">
          <img src="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee.png' ?>"
            srcset="<?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee.png' ?> 1x, <?php echo get_stylesheet_directory_uri(  ) . '/assets/img/warantee@2x.png' ?> 2x"
            alt="Warantee" width="53" height="56">
        </span>
        <span class="text">
          <b>ง่าย สะดวก ปลอดภัย</b><br />
          ตลอด 24 ช.ม.
        </span>
      </div>
    </div>



    <div class="wp-spacer" style="height:60px"></div>




    <!-- มาใหม่ -->
    <section class="alignwide">
      <?php echo do_shortcode('[tm_products_carousel id="latest" title="สินค้าทั้งหมด" title_new="สินค้ามาใหม่" title_bestseller="สินค้าขายดี"]'); ?>
    </section>


    <div class="wp-spacer" style="height:60px"></div>

    
    <!-- มือถือ -->
    <section class="alignwide">
      <?php echo do_shortcode('[tm_products_carousel id="phone" product_cat="phone" title="มือถือ" title_new="มือถือมาใหม่" title_bestseller="มือถือขายดี"]'); ?>
    </section>


    <div class="wp-spacer" style="height:60px"></div>


    <!-- แท็บเล็ต -->  
    <section class="alignwide">
      <?php echo do_shortcode('[tm_products_carousel id="tablet" product_cat="tablet" title="แท็บเล็ต" title_new="แท็บเล็ตมาใหม่" title_bestseller="แท็บเล็ตขายดี"]'); ?>
    </section>


    <div class="wp-spacer" style="height:60px"></div>

    
    <!-- อุปกรณ์เสริม -->
    <section class="alignwide">
      <?php echo do_shortcode('[tm_products_carousel id="accessories" product_cat="accessories" title="อุปกรณ์เสริม" title_new="อุปกรณ์เสริมมาใหม่" title_bestseller="อุปกรณ์เสริมขายดี"]'); ?>
    </section>


    <div class="wp-spacer" style="height:60px"></div>


    <!-- ชิมการ์ด -->
    <section class="alignwide">
      <?php echo do_shortcode('[tm_products_carousel id="simcard" product_cat="sim-card" title="ซิมการ์ด" title_new="ซิมการ์ดเสริมมาใหม่" title_bestseller="ซิมการ์ดขายดี"]'); ?>
    </section>


    <div class="wp-spacer" style="height:60px"></div>


    <figure class="alignwide">
      <a href="#!"><?php echo wp_get_attachment_image( 352, 'full' ); ?></a>
    </figure>



    <div class="wp-spacer" style="height:60px"></div>



    <div class="tm-block-post-grid alignwide">
      <?php
      $blog_posts = [
        (object) [
          'title'   => 'วิธีเลือกซื้อเคสสำหรับไโฟน',
          'excerpt' => 'iPhone เป็นหนึ่งยี่ห้อสมาร์ทโฟนที่ได้รับความนิยมมากทั้งในและต่างประเภท แน่นอนค่ะว่าเรารู้กันดีว่ามันได้รับความนิยมมาอย่างยาวนาน หลายคนเป็นแฟนคลับตัวยงของ iPhone เลยก็ได้เพราะอาจจะใช้ iPhone มาตั้งแต่รุ่น 3G หรือรุ่นดังเดิมมา',
          'image'   => 76,
        ],
        (object) [
          'title'   => 'รีวิว Samsung S21 Ultra 5G',
          'excerpt' => 'Samsung Galaxy S21 Ultra 5G มาพร้อมกับหน้าจอแสดงผลขนาด 6.8 นิ้ว แบบ Dynamic AMOLED 2X ความละเอียด 3200 x 1440 พิกเซล (QHD+) บนดีไซน์หน้าจอแบบ Infinity-O Display ซึ่งรองรับเทคโนโลยี Adaptive 120Hz',
          'image'   => 76,
        ],
        (object) [
          'title'   => '10 อันดับ Power Bank ยี่ห้อไหนดี',
          'excerpt' => 'Power Bank หรือที่หลายคนเรียกว่า แบตเตอรี่สำรอง กลายเป็นไอเทมที่คนยุคใหม่ขาดไม่ได้ในชีวิตประจำวัน ซึ่งเจ้าไอเทมนี้ไม่ได้ใช้ได้กับแค่โทรศัพท์มือถืออย่างเดียว แต่ยังสามารถจ่ายแบตเตอรี่ให้อุปกรณ์อิเล็กทรอนิกส์อื่น ๆ',
          'image'   => 76,
        ],
        (object) [
          'title'   => 'รีวิว Apple Watch Series 6 – คุ้มมั้ย?',
          'excerpt' => 'ก็เปิดตัวและวางจำหน่ายกันเป็นที่เรียบร้อยแล้ว สำหรับ Apple Watch Series 6 สมาร์ตวอทช์รุ่นใหม่จาก Apple ที่เข้ามาทดแทน Series 5 ซึ่งก็ไม่ผิดไปจากข่าวลือที่คาดการณ์กันออกมาครับ คือรูปทรงยังคงเดิม',
          'image'   => 76,
        ],
      ]
      ?>
      <h2 class="block-title">ข่าว/บทความ/รีวิว</h2>
      <ul class="post-grid">
        <?php foreach ($blog_posts as $item) : ?>
        <li class="post-grid__item">
          <article class="post-grid__post">
            <figute class="post-image mb-3">
              <a href="#">
                <?php echo wp_get_attachment_image($item->image, 'large'); ?>
              </a>
              <div class="post-date">
                <span class="date">02</span>
                <span class="month">FEB</span>
              </div>
            </figute>
            <h3 class="post-title">
              <a href="#"><?= $item->title ?></a>
            </h3>
            <div class="post-excerpt">
              <?php echo wpautop( $item->excerpt ); ?>
            </div>
          </article>
        </li>
        <?php endforeach ?>
      </ul>
      <div class="text-center mt-4">
        <a href="<?php the_permalink( get_option('page_for_posts') ) ?>"><?php _e('อ่านบทความเพิ่มเติม', 'treemobile'); ?></a>
      </div>
    </div>



    <div>
      <?php echo do_shortcode('[noptin-form id=354]'); ?>
    </div>

</div>

<?php do_action( 'flatsome_after_page' ); ?>


<?php get_footer(); ?>