import {addAction} from '@wordpress/hooks';
import {makeClass, makeId} from './utils/make-selector';

addAction('lzb.components.PreviewServerCallback.onChange', 'treemobile.blocks', function( props ) {
  if ( 'lazyblock/tm-products-carousel' === props.block ) {
    const blockProductCarousel = require('./blocks/product-carousel');
    blockProductCarousel.editor( document.querySelector( makeClass(props.attributes.blockUniqueClass) ) );
  }
});
