/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/index.admin.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/blocks/product-carousel.js":
/*!**********************************************!*\
  !*** ./assets/js/blocks/product-carousel.js ***!
  \**********************************************/
/*! exports provided: frontend, editor, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "frontend", function() { return frontend; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editor", function() { return editor; });
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! swiper */ "swiper");
/* harmony import */ var swiper__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(swiper__WEBPACK_IMPORTED_MODULE_0__);

/**
 * blockInit
 * @param {HTMLElement} block 
 */

function blockInit(block) {
  var tabs = block.querySelectorAll('[data-bs-toggle="tab"]');
  Array.prototype.forEach.call(tabs, function (tab) {
    tab.addEventListener('shown.bs.tab', function (event) {
      var tabContentHash = event.target.dataset.bsTarget;
      var content = document.querySelector(tabContentHash);

      if (content) {
        var swiperEl = content.querySelector('.swiper-container');

        if (swiperEl) {
          swiperEl.swiper.update();
        }
      }
    });
  });
  var swipers = block.querySelectorAll('.swiper-container');
  Array.prototype.forEach.call(swipers, function (swiper) {
    new swiper__WEBPACK_IMPORTED_MODULE_0___default.a(swiper, {
      slidesPerView: 'auto',
      slidesOffsetBefore: 25,
      slidesOffsetAfter: 25,
      spaceBetween: 25,
      breakpoints: {
        1090: {
          slidesPerView: 5,
          slidesOffsetBefore: 0,
          slidesOffsetAfter: 0
        }
      }
    });
  });
  var buttonPrev = block.querySelector('.products-navigation-button--prev');
  var buttonNext = block.querySelector('.products-navigation-button--next');
  buttonNext.addEventListener('click', function (event) {
    var activeSwiper = block.querySelector('.tab-pane.active .swiper-container');

    if (activeSwiper) {
      var inst = activeSwiper.swiper;
      inst.slideNext();
    }
  });
  buttonPrev.addEventListener('click', function (event) {
    var activeSwiper = block.querySelector('.tab-pane.active .swiper-container');

    if (activeSwiper) {
      var inst = activeSwiper.swiper;
      inst.slidePrev();
    }
  });
}

function frontend() {
  var productsCarousel = document.querySelectorAll('.products-carousel');
  Array.prototype.forEach.call(productsCarousel, function (section) {
    blockInit(section);
  });
}
/**
 * editor
 * @param {HTMLElement} block 
 */

function editor(block) {
  blockInit(block);
}
/* harmony default export */ __webpack_exports__["default"] = ({
  frontend: frontend,
  editor: editor
});

/***/ }),

/***/ "./assets/js/index.admin.js":
/*!**********************************!*\
  !*** ./assets/js/index.admin.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utils_make_selector__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/make-selector */ "./assets/js/utils/make-selector.js");


Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_0__["addAction"])('lzb.components.PreviewServerCallback.onChange', 'treemobile.blocks', function (props) {
  if ('lazyblock/tm-products-carousel' === props.block) {
    var blockProductCarousel = __webpack_require__(/*! ./blocks/product-carousel */ "./assets/js/blocks/product-carousel.js");

    blockProductCarousel.editor(document.querySelector(Object(_utils_make_selector__WEBPACK_IMPORTED_MODULE_1__["makeClass"])(props.attributes.blockUniqueClass)));
  }
});

/***/ }),

/***/ "./assets/js/utils/make-selector.js":
/*!******************************************!*\
  !*** ./assets/js/utils/make-selector.js ***!
  \******************************************/
/*! exports provided: makeClass, makeId, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "makeClass", function() { return makeClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "makeId", function() { return makeId; });
/**
 * isString
 * ตรวจสอบว่า string เป็น string จริง ๆ หรือไม่
 * @param {string} string
 */
function isString(string) {
  return typeof string === 'string';
}
/**
 * isId
 * ตรวจสอบว่าใช่ string ที่มีรูปแบบเป็น id selector อยู่แล้วหรือไม่
 * @param {string} string;
 */


function isId(string) {
  return string.substr(0, 1) === '#';
}
/**
 * isClass
 * ตรวจสอบว่าใช่ string ที่มีรูปแบบเป็น class selector อยู่แล้วหรือไม่
 * @param {string} string;
 */


function isClass(string) {
  return string.substr(0, 1) === '.';
}

;
/**
 * makeClass
 * @param {string} string 
 * @returns {string} class selector
 */

function makeClass(string) {
  var output = isClass(string) ? string.substr(1, string.length) : string;
  return ".".concat(output);
}
/**
 * makeId
 * @param {string} string 
 * @returns {string} id selector
 */

function makeId(string) {
  var output = isId(string) ? string.substr(1, string.length) : string;
  return "#".concat(output);
}
/* harmony default export */ __webpack_exports__["default"] = ({
  makeClass: makeClass,
  makeId: makeId
});

/***/ }),

/***/ "@wordpress/hooks":
/*!***************************!*\
  !*** external "wp.hooks" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = wp.hooks;

/***/ }),

/***/ "swiper":
/*!*************************!*\
  !*** external "Swiper" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = Swiper;

/***/ })

/******/ });
//# sourceMappingURL=treemobile.admin.js.map