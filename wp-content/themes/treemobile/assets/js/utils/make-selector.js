/**
 * isString
 * ตรวจสอบว่า string เป็น string จริง ๆ หรือไม่
 * @param {string} string
 */
 function isString(string) {
  return typeof string === 'string';
}

/**
 * isId
 * ตรวจสอบว่าใช่ string ที่มีรูปแบบเป็น id selector อยู่แล้วหรือไม่
 * @param {string} string;
 */
function isId(string) {
  return string.substr(0, 1) === '#';
}

/**
 * isClass
 * ตรวจสอบว่าใช่ string ที่มีรูปแบบเป็น class selector อยู่แล้วหรือไม่
 * @param {string} string;
 */
function isClass(string) {
  return string.substr(0, 1) === '.';
};

/**
 * makeClass
 * @param {string} string 
 * @returns {string} class selector
 */
export function makeClass(string) {
  let output = isClass(string) ? string.substr(1, string.length) : string;
  return `.${output}`;
}

/**
 * makeId
 * @param {string} string 
 * @returns {string} id selector
 */
export function makeId(string) {
  let output = isId(string) ? string.substr(1, string.length) : string;
  return `#${output}`;
}

export default {
  makeClass,
  makeId
}