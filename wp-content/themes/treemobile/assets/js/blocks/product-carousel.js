import Swiper from 'swiper';

/**
 * blockInit
 * @param {HTMLElement} block 
 */
function blockInit( block ) {
  let tabs = block.querySelectorAll('[data-bs-toggle="tab"]');

  Array.prototype.forEach.call(tabs, tab => {
    tab.addEventListener('shown.bs.tab', function(event) {
      let tabContentHash = event.target.dataset.bsTarget;
      let content = document.querySelector(tabContentHash);
      if (content) {
        let swiperEl = content.querySelector('.swiper-container');
        if (swiperEl) {
          swiperEl.swiper.update();
        }
      }
    });
  })

  let swipers = block.querySelectorAll('.swiper-container');
  Array.prototype.forEach.call(swipers, swiper => {
    new Swiper(swiper, {
      slidesPerView: 'auto',
      slidesOffsetBefore: 25,
      slidesOffsetAfter: 25,
      spaceBetween: 25,
      breakpoints: {
        1090: {
          slidesPerView: 5,
          slidesOffsetBefore: 0,
          slidesOffsetAfter: 0,
        }
      }
    });
  });

  let buttonPrev = block.querySelector('.products-navigation-button--prev');
  let buttonNext = block.querySelector('.products-navigation-button--next');

  buttonNext.addEventListener('click', event => {
    let activeSwiper = block.querySelector('.tab-pane.active .swiper-container');
    if (activeSwiper) {
      let inst = activeSwiper.swiper;
      inst.slideNext();
    }
  });
  
  buttonPrev.addEventListener('click', event => {
    let activeSwiper = block.querySelector('.tab-pane.active .swiper-container');
    if (activeSwiper) {
      let inst = activeSwiper.swiper;
      inst.slidePrev();
    }
  });
}

export function frontend() {
  let productsCarousel = document.querySelectorAll('.products-carousel');

  Array.prototype.forEach.call(productsCarousel, section => {
    blockInit( section );
  });
}

/**
 * editor
 * @param {HTMLElement} block 
 */
export function editor( block ) {
  blockInit( block );
}

export default {
  frontend,
  editor
}