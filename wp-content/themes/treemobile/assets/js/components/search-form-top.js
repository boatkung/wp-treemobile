import $ from 'jquery';

$('.search-form-top').each((i, form) => {
  let $form = $(form);
  let $dropdown = $('.search-dropdown');
  let $button = $dropdown.find('.search-categories');
  let $menu = $dropdown.find('.dropdown-menu');
  let $catField = $form.find('[name="product_cat"]');

  $menu.on('click', 'a.dropdown-item', ev => {
    ev.preventDefault();

    let $this = $(ev.target);
    let $sibs = $this.parent().siblings();
    $this.addClass('active')
    $sibs.children('.active').removeClass('active');

    let term = ev.target.innerText;
    let termSlug = ev.target.dataset.termSlug;
    $button.text(term);
    $catField.val(termSlug);
  });

  if (location.search.indexOf('product_cat=') >= 0) {
    let qs = location.search.substr(1, location.search.length).split('&');
    let q = qs.filter(t => t.indexOf('product_cat') >= 0)[0];
    let term = q.split('=')[1];
    
    let $active = $menu.find(`[data-term-slug="${term}"]`);
    let $sibs = $active.parent().siblings();
    $active.addClass('active');
    $sibs.children('.active').removeClass('active');

    let termName = $active.text();
    $catField.val(term);
    $button.text(termName);
  }
});
