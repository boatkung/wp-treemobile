<?php
$private      = __('Private');
$private_note = __('สินค้านี้จะเห็นเฉพาะผู้ที่มีสิทธิ์เข้าถึงเท่านั้น', 'treemobile');
$post_status  = get_post_status();

$product  = wc_get_product( get_the_ID() );
$name     = $product->get_name();
$price    = wc_price( $product->get_price() );
?>

<div <?php post_class('product-item'); ?>>
  <figure class="product-item__image">
    <a href="<?php the_permalink(); ?>">
      <?php echo woocommerce_get_product_thumbnail(); ?>
    </a>
  </figure>

  <h2 class="product-item__title">
    <?php
    if ( $post_status !== 'publish' ) {
      echo "<span title=\"{$private_note}\" class=\"text-warning\">({$private})</span> ";
    }
    ?>
    <?= $name ?>
  </h2>

  <div class="product-item__info d-flex flex-wrap justify-content-between align-items-center">
    <div class="price">
      <?php _e('ราคา', 'treemobile'); ?> <?= $price ?>
    </div>

    <div class="call-to-action">
      <a class="add_to_cart_button button primary is-flat is-small" href="<?php echo $product->add_to_cart_url(); ?>">
        <?php echo $product->add_to_cart_text(); ?>
      </a>
    </div>
  </div>
</div>