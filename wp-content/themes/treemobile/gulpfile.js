const {src, dest, task, watch, series, parallel} = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const postcssPresetEnv = require('postcss-preset-env');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const webpack = require('webpack-stream');

task('style', () => {
  return src('./assets/scss/index.scss', {sourcemaps:true})
    .pipe(plumber())
    .pipe(sass.sync())
    .pipe(postcss([
      postcssPresetEnv({
        autoprefixer: true
      })
    ]))
    .pipe(rename({
      basename: 'treemobile',
    }))
    .pipe(dest('./assets/css', {sourcemaps:'.'}))
});

task('style-admin', () => {
  return src('./assets/scss/index.admin.scss', {sourcemaps:true})
    .pipe(plumber())
    .pipe(sass.sync())
    .pipe(postcss([
      postcssPresetEnv({
        autoprefixer: true
      })
    ]))
    .pipe(rename({
      basename: 'treemobile',
      suffix: '.admin',
    }))
    .pipe(dest('./assets/css', {sourcemaps:'.'}))
});

task( 'script', () => {
  const webpackConfig = require('./webpack.config');

  return src('./assets/js/index.js')
    .pipe(webpack({
      ...webpackConfig,
      mode: 'development',
    }))
    .pipe(dest('./assets/js'))
} );

task( 'script-admin', () => {
  const webpackConfig = require('./webpack.config');

  return src('./assets/js/index.admin.js')
    .pipe(webpack({
      ...webpackConfig,
      mode: 'development',
      output: {
        filename: 'treemobile.admin.js',
      }
    }))
    .pipe(dest('./assets/js'))
} );

task('watch', () => {
  let watchOptions = {
    interval: 150,
    ignored: ['node_modules']
  }

  watch([
    './assets/scss/**/*.scss',
  ], watchOptions, parallel('style', 'style-admin'));

  watch([
    './assets/js/index.js',
    './assets/js/blocks/*.js',
    './assets/js/components/*.js',
  ], watchOptions, series('script'));

  watch([
    './assets/js/index.admin.js',
    './assets/js/blocks/*.js',
    './assets/js/components/*.js',
  ], watchOptions, series('script-admin'));
});