<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'shop_treemobile' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'wYH*%ak4Y+]LegdAu}_QLt-tpKRK4=1<PZM+c=y~S}6hRNbo4 %s_%8Zh>VX$L/I' );
define( 'SECURE_AUTH_KEY',  'B.p@XgY:k;=~?hwURyR>F1hYH1/, kT$}@dw]|E0b>8tpIFUcFM0Mi_~g{<m@mk>' );
define( 'LOGGED_IN_KEY',    'iU<r.@URee)OXmll|-GtzkOFgSiFj-P&iFgaHCt/S|rxatb{p}f-8Y PDm;!WWcR' );
define( 'NONCE_KEY',        '?^9|l8]zz6~@RO^0K{n-ZaJ<,~^5BaA+,~F;Mvy=$}: Q{vrL/zq(n<!r[B/I[#7' );
define( 'AUTH_SALT',        'wGND&mci J/1UJBUo^i7E-RD(l[]v62g2OpG$elVNH]JB^!Qt;6nD|c_-noH980L' );
define( 'SECURE_AUTH_SALT', '-C8|R`qvqV8@#fF=+27t$-h. Y[Mon%O.C%hQM:e|l?Vt%;v%8e(rd2<fqM0&brC' );
define( 'LOGGED_IN_SALT',   'Os/w`<}HZI*i33AL`t.b=N1o9nWJsYCB|b91?xHV~z06<cPk#,uXW33TO-Y>4J~u' );
define( 'NONCE_SALT',       'S$4qF,qs[+ti7^qJIWA_j!ywN;>]WaN#(,DJvYJQ}M=<7F+<eJ,.5U~0;VB(o 3@' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'tm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );

define( 'SCRIPT_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
